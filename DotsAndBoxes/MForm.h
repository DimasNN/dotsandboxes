#pragma once
//#include "engine.h"
#include "board.h"
#include <vector>

namespace DotsAndBoxes {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	using namespace System::Threading;//������ �����


	/// <summary>
	/// ������ ��� MForm
	/// </summary>
	public ref class MForm : public System::Windows::Forms::Form
	{
	public:		
		
		MForm(void)
		{
			InitializeComponent();
			//engine = new Engine; //�������� ������
			
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MForm()
		{
			if (components)
			{
				delete components;
			}
			
		}
	private: System::Windows::Forms::Button^  btnStart;
	private: System::Windows::Forms::Button^  btExit;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
	private: System::Windows::Forms::Button^  btCreate;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  tbHeight;
	private: System::Windows::Forms::TextBox^  tbWidth;
	private: System::Windows::Forms::TextBox^  tbDepth;
	private: System::Windows::Forms::Label^  label3;
	protected:

	private:
		
		
		/// <summary>
		/// ��������� ���������� ������������.
		
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->btnStart = (gcnew System::Windows::Forms::Button());
			this->btExit = (gcnew System::Windows::Forms::Button());
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->btCreate = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->tbHeight = (gcnew System::Windows::Forms::TextBox());
			this->tbWidth = (gcnew System::Windows::Forms::TextBox());
			this->tbDepth = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// btnStart
			// 
			this->btnStart->Location = System::Drawing::Point(12, 12);
			this->btnStart->Name = L"btnStart";
			this->btnStart->Size = System::Drawing::Size(75, 23);
			this->btnStart->TabIndex = 0;
			this->btnStart->Text = L"Start";
			this->btnStart->UseVisualStyleBackColor = true;
			this->btnStart->Click += gcnew System::EventHandler(this, &MForm::btnStart_Click);
			// 
			// btExit
			// 
			this->btExit->Location = System::Drawing::Point(12, 70);
			this->btExit->Name = L"btExit";
			this->btExit->Size = System::Drawing::Size(75, 23);
			this->btExit->TabIndex = 1;
			this->btExit->Text = L"Exit";
			this->btExit->UseVisualStyleBackColor = true;
			this->btExit->Click += gcnew System::EventHandler(this, &MForm::btExit_Click);
			// 
			// backgroundWorker1
			// 
			this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MForm::backgroundWorker1_DoWork);
			// 
			// btCreate
			// 
			this->btCreate->Location = System::Drawing::Point(12, 41);
			this->btCreate->Name = L"btCreate";
			this->btCreate->Size = System::Drawing::Size(75, 23);
			this->btCreate->TabIndex = 2;
			this->btCreate->Text = L"Create";
			this->btCreate->UseVisualStyleBackColor = true;
			this->btCreate->Click += gcnew System::EventHandler(this, &MForm::btCreate_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 100);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(36, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"height";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(13, 129);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(32, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"width";
			// 
			// tbHeight
			// 
			this->tbHeight->Location = System::Drawing::Point(55, 100);
			this->tbHeight->Name = L"tbHeight";
			this->tbHeight->Size = System::Drawing::Size(32, 20);
			this->tbHeight->TabIndex = 5;
			this->tbHeight->Text = L"3";
			// 
			// tbWidth
			// 
			this->tbWidth->Location = System::Drawing::Point(55, 126);
			this->tbWidth->Name = L"tbWidth";
			this->tbWidth->Size = System::Drawing::Size(32, 20);
			this->tbWidth->TabIndex = 6;
			this->tbWidth->Text = L"3";
			// 
			// tbDepth
			// 
			this->tbDepth->Location = System::Drawing::Point(55, 152);
			this->tbDepth->Name = L"tbDepth";
			this->tbDepth->Size = System::Drawing::Size(32, 20);
			this->tbDepth->TabIndex = 7;
			this->tbDepth->Text = L"2";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(13, 155);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(34, 13);
			this->label3->TabIndex = 8;
			this->label3->Text = L"depth";
			// 
			// MForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(465, 408);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->tbDepth);
			this->Controls->Add(this->tbWidth);
			this->Controls->Add(this->tbHeight);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->btCreate);
			this->Controls->Add(this->btExit);
			this->Controls->Add(this->btnStart);
			this->Name = L"MForm";
			this->Text = L"MForm";
			this->Load += gcnew System::EventHandler(this, &MForm::MForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private:
		//���������
		
		int height, width;		//������ � ������	
		int depth; //�������
		char created = 0; //���� ������� ������ create
		char countSysElements; //������� ������, ������� �� ���� �������
		char whoStep = 0; //��� ����� �������-0, ��������� -1
		array<array<Button^>^>^ msv;	 //������������ ������ 
		array<array<Button^>^>^ msh;	 //�������������� ������
		Board *brd = nullptr;


		void BtCreate();
		void AddBtMarker(int posH,int posW,char color); //0 to  heigh and 0 to width  color = g-green or r-red
		void UpdateMarkers(); //�������� ������� ���� �� �����

	String^ getHeap()
		{
			//srand(time(0));
			String^ str;
			int a = rand() % 32 + 1;
			str = a.ToString();
			return str;
		}
			
				
			//�������� �� ������� ������� �� �������. ������ �������
	private: System::Void buttonsClick(System::Object^  sender, System::EventArgs^  e) {

	

		Button^ b = (Button^)sender;
		//MessageBox::Show(b->Name);
		int posV, posH, type; //type h-������������ w-��������������
		
		String^ name = (b->Name);  //debug
		std::cout << (char)name[0]; //debug
		type = b->Name[0];  //���������, ������� ������ ������ �� �������� ������
		posV = (int)(b->Name[3])-48;
		posH = (int)(b->Name[6]) - 48;

		std::cout << posV << posH << "\n";
		
		//char color='b';
		//if (whoStep) {
		//	color = 'r';//���� ��� ����������
		//}
		//���������� ��� � ������
		Step bufStep;
		b->BackColor = BackColor.Black;
		if (type == 'v')
			bufStep.isVert= true;
		else
			bufStep.isVert = false;
		bufStep.height = posV;
		bufStep.width = posH;
		bufStep.isComp = -1;

		brd->GoStepAndGetRate(bufStep);
		//UpdateMarkers(); //�������� ������� ����
		if (!brd->GetLastStep().isFill)
		{
			brd->RunMinMax(1, Convert::ToInt32(tbDepth->Text));
			
		}
			

		//bufStep = brd->GetLastStep();

		//PrintOnButton(bufStep.height, bufStep.width, brd->board[bufStep.height][bufStep.width].c);
		UpdateMarkers();
		//b->ForeColor = BackColor.Black;
		
	}
	private: Void PrintOnButton(int height, int width,bool isVert) {
		//Button^ b;
		//String^ str;
		if (isVert){
			msv[height][width]->BackColor = BackColor.Black;
		}
		else{
			msh[height][width]->BackColor = BackColor.Black;
		}

		/*if (isVert) str = "v_v";
			else str = "h_v";

			str += height.ToString();
			str += "_h" + width.ToString();

			Controls->Find(str, 0)[0]->BackColor = BackColor.Black;*/
	}
	private: System::Void btnStart_Click(System::Object^  sender, System::EventArgs^  e) {
		Graphics^ graph = this->CreateGraphics();
		graph->DrawLine(Pens::Brown, 0, 0, 10, 10);

	}
	private: System::Void MForm_Load(System::Object^  sender, System::EventArgs^  e) {
		countSysElements = Controls->Count; //���� ��� ������� ���� ��������� ������� �� ���� ������� 
											//�����, ����� ������������� ��������� ������ �������� �� �����
		height = Convert::ToByte(tbHeight->Text);
		width = Convert::ToByte(tbWidth->Text);
	}
	



	private: System::Void btExit_Click(System::Object^  sender, System::EventArgs^  e) {

		//backgroundWorker1->RunWorkerAsync();
		//MessageBox::Show(L"This is the main thread");
		
		//Controls->RemoveAt(7);
		//MessageBox::Show(countSysElements.ToString() );
		//AddBtMarker(1, 1, 'r'); //���������� ������� ������ ��� ����������
		PrintOnButton(1, 0, 1);
		//if (brd) delete brd;
		//PrintOnButton(0, 0, 0);

	}
			 
			 //������������ ����������
	private: System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {	
		
		MessageBox::Show(L"This is the async thread");
	}
private: System::Void btCreate_Click(System::Object^  sender, System::EventArgs^  e) {
	BtCreate();
	if (brd)  delete brd;	
		brd = new Board(height, width, Convert::ToInt32(tbDepth->Text));
	Step bufStep;
	brd->RunMinMax(1,  Convert::ToInt32(tbDepth->Text)); //koeff 1=comp -1=user
	//bufStep = brd->GetLastStep();
	
	//PrintOnButton(bufStep.height, bufStep.width, brd->board[bufStep.height][bufStep.width].ver );
	UpdateMarkers();
}



};
}
