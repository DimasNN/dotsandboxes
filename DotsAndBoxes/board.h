#pragma once
#include <iostream>
#include <vector>
struct Box {	
	 char c=0;	//����� � ������	0-���, 1-������, 2-�����
	bool hor=0;	//�������������� �������	0-���, 1-����, 2-�����
	bool ver=0;	//������������ �������	0-���, 1-����, 2-�����//  __
	//��� ������� � ������� ����
	bool validVer = 1; //�������� �� ������������ �������
	bool validHor = 1; //�������� �� �������������� 
	// | x		-������
	//
	//�� ���� ������ ������ ����� ������� ���� ��������
};

struct Step {
	//int depth;		//������� ��������
	//int num;		//����� ����
	//int rating;		// ��������� ������, ������� ����
	int height=0;		//������� ����
	int width=0;		//
	bool isVert=0;	//��� �������, ������������=true
	//bool isFillLeft=0;	//���������� �� 
	//bool isFillRight = 0;	//���������� ��
	//bool isFillUp = 0;	//���������� ��
	//bool isFillDown = 0;	//���������� ��
	char isComp=1;	//��������� �����?				0 ��� �����, 1=���������   -1=player
	char isFill = 0; //����� ������ �������� ������ 0-�� �����   1=�������	  -1= �������
};

 class Board //����� �������� ��������� �����
{
	
	int height, width;
	int maxSteps; //
	int depth;
	int stepsCount;
	int MaxFill;	//������������ ����� ��� ����������
	int cntFillComp = 0;	//����� ����������� �����������
	int cntFiilPlayer = 0;	//����� ����������� �������
	//Step lastStep;
	std::vector<Step> Steps;

	unsigned int debCountFor = 0;	//�������
	unsigned int debCount = 0;	//�������
	
public:
	//Box **board; //��������� �� ��������� ������
	Box board[4][4];
	Board(int _heigt, int _weight,int _depth);	
	
	int ValidStep(int ver, int hor, bool isVert);
	Step NextStep(int num); //������� ��� ����� num
	Step GetLastStep() { return Steps.back(); }
	void InputStep(int height, int width, int depth);
	int GoStepAndGetRate(Step  step);		//��������� ���
	void Board::BackStep();		//��������� ���
	float RunMinMax(int isComp, int recursLevel);


	void DeleteBoard(); //������� ������
	~Board(){ //DeleteBoard(); 
	}
	
};

