#include "board.h"

Board::Board(int _height, int _width, int _depth)
{	
	height = _height;	//������, ����� �����������
	width = _width;		//������, ����� �����������
	depth = _depth;
	maxSteps = 2 * (height + 1)*(width + 1);//2 * (_height)*(_width)+_height + _width; //������������ ����� ����� � ������� �� �������� �������� NextStep
	//�������� ����� ��� ����� (��������� ������)
	/*
	if (_height+1 > 0 && _width+1 > 0){ //�������� �� ��������� ���������
		board = new Box*[_height+1];
		for (int i = 0; i < _width+1; ++i)
			board[i] = new Box[_width+1];
	}
	*/
	//�������� ��������� ������� ��-�� ����������� �������� ������� �����
	for (int i = 0; i <= height; i++)
		board[i][width].validHor = 0;
	for (int i = 0; i <= width; i++)
		board[height][i].validVer = 0;
}

void Board::DeleteBoard()
{	
		//�������� ����� �����
		if (board){
			for (int i = 0; i < height + 1; ++i) {
				delete board[i];
			}
			delete *board;
		}

	
}
//���������� ������ �������� ��������
float Board::RunMinMax(int isComp, int recursLevel) //isComp 1=comp -1=user
{
	++debCount;
	//����������� ��������
	if (recursLevel <= 0 ) {
		return 0;
	}
	/*if (depth > 2 * height*width + height + width - Steps.size())
	{
			  depth = 2 * height*width + height + width - Steps.size()-1;
		//recursLevel = 2 * height*width + height + width - Steps.size();
	}*/
		
			
	
	//int isComp = step.isComp;
	
	float koeff;	
	float koeffComp = 3;
	float koeffPlayer = 3;
	int rate=0;
	int MaxRate = INT_MIN; 
	int numBestStep = 0;
	Step bestStep, step;
	//int ii;
	//int numBestStep;

	for (int i = 0; i < maxSteps-2; ++i)
	{	
		++debCountFor;
		step = NextStep(i); //�������� ���
		if (step.isComp == 0) //���� �� �������� ���
		{
			continue;
		} 
		
		//������� �� ����������� 
		if (isComp==1)
			koeff = koeffComp;
		else
			koeff = koeffPlayer;

		rate = GoStepAndGetRate(step) * isComp;		
		
		if (rate == 0){
			isComp *= -1; //��� ����� ��������� ���( ����������� ��������) 			
		}
		
			
		//��� ��������
		rate += koeff *(recursLevel+1)* RunMinMax(isComp, recursLevel - 1); //���� ��� ���������� ���������� ��� ������ ����
					
		//���������� ������ ���
		if (rate > MaxRate)
		{
			MaxRate = rate;
			numBestStep = i;
			bestStep=step;
		}
		//�������� ��� (������������ �����)
		BackStep();
		//
	}
	
	// ������ ������ ���
	 //step = NextStep(numBestStep); //������� � ������� ���� � ������ ����� �����
	
	//������� ��� ������ ����� ������ ������� �� �������� � ������� 
	if (recursLevel == depth){
		//bestStep.isComp = 1;
		std::cout << "step " << bestStep.height << bestStep.width << bestStep.isVert << std::endl;
		std::cout << "rate " << MaxRate << std::endl; //������� ��� ��� ���� ���������
		GoStepAndGetRate(bestStep);
		while (GetLastStep().isFill == 1)
			RunMinMax(1,depth);
	}
	  
	return MaxRate; //������� �������
	
}

//�������� �������� �� ��� �� ����������� �������
int Board::ValidStep(int i, int j, bool isVert)  //i =height  j =width
{

	if (isVert)
	{
		if (i < height)
			if (!board[i][j].ver) //�������� ��� �� ���
				return 1;
	}
	else
	{
		if (j < width)
			if (!board[i][j].hor) //�������� ��� �� ���
				return 1;
	}
	return 0; //��� ���������� ��� ��� ������
}

Step Board::NextStep(int num)  
{
	Step step;
	int isVert = num % 2; //����� ����� 0=hor 1=vert
	int i = num / 2;
	if (isVert == 0)	//��� � �������������� ��������
		if (i / 2 <(maxSteps / 2 - 1) )
		{
			step.isVert = false;
			step.height = i / (width + 1);
			step.width = i % (width + 1);
			if (step.width < width) //�������� �������� �� ���
				if (!board[step.height][step.width].hor) //�������� ��� �� ���				
					return step;
		}
	if (isVert == 1)	//��� � ������������ ��������
	if (i / 2 <(maxSteps / 2 - width - 1))
	{
		step.isVert = true;
		step.height = i / (width + 1);
		step.width = i % (width + 1);
		if (step.height < height) //�������� �������� �� ���
			if (!board[step.height][step.width].ver) //�������� ��� �� ���					
				return step;
	}


	//std::cout << num << " --> " << num / 2 << std::endl;
	step.isComp = 0; //���� ������������� ����
	return step;
	
}



//���������� 1 ���� �� ���� ���� ��������� �����������
//� ������ ���
//��������� ��� � �����
int Board:: GoStepAndGetRate(Step  step)
{
	bool isFillLeft=0, isFillRight=0, isFillUp=0, isFillDown=0;

	//������ ���
	int rate = 0;
	//���� ���������� ������������ �������
	if (step.isVert == true)
	{	//����	 __
		//		|__
		if (step.height  < height && step.width  > 0 &&   //������� �������
			board[step.height][step.width - 1].hor &&			//������ ������ ����������		___			
			board[step.height][step.width - 1].ver &&			//����� �������					|*
			board[step.height + 1][step.width - 1].hor) {				//���  ����������		 ---
			isFillLeft = true; //��������� ����������� � ���������� � ����
			rate ++;
		}
		//�����  __
		//		 __|
		if (step.height  < height && step.width  < width &&   //������� �������
			board[step.height][step.width].hor &&		//������ �������� ����������		  __			
			board[step.height][step.width + 1].ver &&			//			������ �������	  * |
			board[step.height + 1][step.width].hor)	{		//��� ����������			      ---
			isFillRight = true;
			rate++;
		}
	}
	if (step.isVert == false)	//���� ���������� ��������������
	{	//����   __
		//		|  |
		if (step.height  > 0 && step.width  < width &&   //������� �������
			board[step.height - 1][step.width].ver &&			//����� �������				|*		
			board[step.height - 1][step.width].hor &&			//	���� ����������			___
			board[step.height - 1][step.width + 1].ver){		   //������ �������			   |
			isFillUp = true;
			rate++;
		}
		//��� |__|			   
		if (step.height  < height && step.width  < width &&   //������� �������
			board[step.height][step.width].ver &&					//����� �������		|*		
			board[step.height + 1][step.width].hor &&				//	��� ����������   ___
			board[step.height][step.width + 1].ver)	{		 //������ �������			 |
			isFillDown = true;
			rate++;
		}
	}

	//����� ��������� ���
	char id = 0;
	if (isFillLeft || isFillRight || isFillUp || isFillDown) {
		if (step.isComp==1) { id = 1; }
		else { id = -1; }
	}

	if (isFillDown)
		board[step.height][step.width].c = id; 
	if ( isFillRight)
		board[step.height][step.width].c = id; 
	if( isFillUp )
		board[step.height-1][step.width].c = id;
	if (isFillLeft)
		board[step.height][step.width - 1].c = id; 
	
	board[step.height][step.width].ver |= step.isVert;
	board[step.height][step.width].hor |= (!step.isVert);

	step.isFill = id;
	//������� ����������� ������
	if (id == 1) cntFillComp++;
	if (id == -1) cntFiilPlayer++;

	Steps.push_back(step);//�������� �������� ��� � ������ �����


	stepsCount++;
	return rate;
}		

//�������� 1 ��� �� �����
void Board:: BackStep()
{
	if (Steps.size()){
		Step step = Steps.back();

		//������ ����������� ����� ��������
		if (step.isVert){ //���� ������������ ������� 
			if (step.width >0){
				board[step.height][step.width - 1].c = 0;
			}
			if (step.width < width){
				board[step.height][step.width].c = 0;
			}
		}
		if (!step.isVert){ //���� �������������� ������� 
			if (step.height >0){
				board[step.height-1][step.width ].c = 0;
			}
			if (step.height < height){
				board[step.height][step.width].c = 0;
			}
		}
		

		board[step.height][step.width].ver ^= step.isVert;
		board[step.height][step.width].hor ^= (!step.isVert);
		Steps.pop_back(); //������ ��� �� ������� �����
		stepsCount--;
	}
}		
